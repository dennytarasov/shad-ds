package lib

import "time"

const MaxMessageDelay = 1000
const MaxTimestamp = 2000
const MaxSendTimestamp = MaxTimestamp - MaxMessageDelay
const MaxNodeCount = 10000
const TestTimeout = 5 * time.Second

type SentMessage struct {
	Message          Message
	Source           int
	Destination      int
	SendTimestamp    int
	DeliverTimestamp int
}

type SendRequest struct {
	Timestamp   int
	Source      int
	Destination int
	Value       string
}

type Validator struct {
	Timestamp int
	Node      int
	Source    int
	Validate  func(values []string) bool
}

// TestDescriptor
type TestDescriptor struct {
	Author   string
	TestName string

	NodeCount    int
	SendRequests []SendRequest
	Validators   []Validator

	// DelayFunc returns deliver timestamp of a given SentMessage.
	DelayFunc func(message SentMessage) int
}
