package lib

import "time"

const MaxTimestamp = 1000000
const MaxNodeCount = 1000
const TestTimeout = 5 * time.Second
const TestSuitTimeout = 10 * time.Second

type SendRequest struct {
	HappenedBeforeMessages []int
	MessageId              int
	Source                 int
}

type FailureDescriptor struct {
	HappenedBeforeMessages []int
	Source                 int
	BlockedDirections      []int
}

type SentMessage struct {
	Message          interface{}
	Source           int
	Destination      int
	SendTimestamp    int
	DeliverTimestamp int

	// If true, this message notifies destination peer that source peer is dead.
	// Message field should be ignored.
	Tombstone bool
}

type TestDescriptor struct {
	Author   string
	TestName string

	NodeCount          int
	SendRequests       []SendRequest
	FailureDescriptors []FailureDescriptor

	// DelayFunc returns deliver timestamp of a given SentMessage.
	// It should not be less than original DeliverTimestapm of a passed message.
	DelayFunc func(message SentMessage) int
}
