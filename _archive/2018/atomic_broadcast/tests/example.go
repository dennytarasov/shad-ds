package tests

import "gitlab.com/slon/shad-ds/atomic_broadcast/lib"

func NoDelayFunc(msg lib.SentMessage) int {
	return msg.DeliverTimestamp
}

func InfDelayFunc(msg lib.SentMessage) int {
	return msg.DeliverTimestamp + 10000*lib.AvgDeliveryTime
}

var ExampleTests = []lib.TestDescriptor{
	{
		Author:    "psushin@yandex-team.ru",
		TestName:  "example test 1",
		NodeCount: 3,
		Segments: []lib.TestSegment{
			{
				StableBroadcasts: []lib.BroadcastRequest{
					{
						Timestamp: 1000,
						MessageId: 1,
						Source:    0,
					},
					{
						Timestamp: 2000,
						MessageId: 2,
						Source:    1,
					},
					{
						Timestamp: 3000,
						MessageId: 3,
						Source:    2,
					},
				},
				StableDelayFunc: NoDelayFunc,
				UnstableBroadcasts: []lib.BroadcastRequest{
					{
						Timestamp: 1000,
						MessageId: 4,
						Source:    0,
					},
					{
						Timestamp: 2000,
						MessageId: 5,
						Source:    1,
					},
					{
						Timestamp: 3000,
						MessageId: 6,
						Source:    2,
					},
				},
				UnstableDelayFunc: InfDelayFunc,
				DeathEvents:       []lib.NodeEvent{},
				ReviveEvents:      []lib.NodeEvent{},
			},
		},
	},
	{
		Author:    "psushin@yandex-team.ru",
		TestName:  "example test 3",
		NodeCount: 3,
		Segments: []lib.TestSegment{
			{
				StableBroadcasts: []lib.BroadcastRequest{
					{
						Timestamp: 1000,
						MessageId: 1,
						Source:    0,
					},
					{
						Timestamp: 2000,
						MessageId: 2,
						Source:    0,
					},
				},
				StableDelayFunc: NoDelayFunc,
				UnstableBroadcasts: []lib.BroadcastRequest{
					{
						Timestamp: 18000,
						MessageId: 3,
						Source:    1,
					},
					{
						Timestamp: 20000,
						MessageId: 4,
						Source:    2,
					},
				},
				UnstableDelayFunc: NoDelayFunc,
				DeathEvents: []lib.NodeEvent{
					{
						Timestamp: 1000,
						Node:      0,
					},
					{
						Timestamp: 2000,
						Node:      1,
					},
				},
				ReviveEvents: []lib.NodeEvent{
					{
						Timestamp: 15000,
						Node:      1,
					},
				},
			},
			{
				StableBroadcasts: []lib.BroadcastRequest{
					{
						Timestamp: 1000,
						MessageId: 5,
						Source:    1,
					},
				},
				StableDelayFunc:    NoDelayFunc,
				UnstableBroadcasts: []lib.BroadcastRequest{},
				UnstableDelayFunc:  InfDelayFunc,
				DeathEvents:        []lib.NodeEvent{},
				ReviveEvents:       []lib.NodeEvent{},
			},
		},
	},
}
