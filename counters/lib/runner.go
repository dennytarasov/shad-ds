package lib

import (
	"fmt"
	"runtime/debug"
	"testing"
	"time"

	"gitlab.com/slon/shad-ds/counters/interfaces"
	"go.uber.org/atomic"
	"go.uber.org/zap"
	"go.uber.org/zap/zaptest"
)

func RunTest(
	test TestDescriptor,
	countersCtor interfaces.CountersServiceCtor,
	log *zap.SugaredLogger) (err error) {

	defer func() {
		if e := recover(); e != nil {
			err = fmt.Errorf("%v\nStack:\n%s\n", e, debug.Stack())
		}
	}()

	as := &ActorSystem{
		delayMessage:   test.DelayMessage,
		actors:         make(map[string]interfaces.Actor),
		actorIsolated:  make(map[string]struct{}),
		clients:        make(map[string]*Client),
		queuedMessages: []SentMessage{},
		sentMessages:   make(chan SentMessage, 100),

		// Client callbacks to be executed in main actor system thread.
		clientCallbacks:        make(chan func(), 100),
		timestamp:              atomic.NewInt32(0),
		timestampDeliveryLimit: atomic.NewInt32(-1),
		log: log,
	}

	if test.NodeCount <= 0 {
		panic("node count must be positive")
	}
	for nodeIndex := 0; nodeIndex < test.NodeCount; nodeIndex++ {
		actorId := interfaces.GetCounterActorId(nodeIndex)
		actorContext := ActorContext{
			actorSystem: as,
			id:          actorId,
		}
		actor := countersCtor(&actorContext, nodeIndex, test.NodeCount)
		err = as.registerActor(actorId, actor)
		if err != nil {
			panic(fmt.Errorf("failed to register counters service for node %v", nodeIndex))
		}
	}

	clientResults := make(chan error, len(test.Clients))

	for _, client := range test.Clients {
		c := client
		go func() {
			clientResults <- c(as)
		}()
	}

	finishedClients := 0

	finishSimulation := func() bool {
		return as.getCurrentTimestamp() > MaxTimestamp ||
			(finishedClients == len(test.Clients) && len(as.queuedMessages) == 0)
	}

	for !finishSimulation() {
		select {
		case sentMessage := <-as.sentMessages:
			as.enqueueSentMessage(sentMessage)

		case cb := <-as.clientCallbacks:
			cb()

		case clientResult := <-clientResults:
			if clientResult != nil {
				return clientResult
			}
			finishedClients += 1

		default:
			if !as.deliverNextMessage() {
				time.Sleep(time.Millisecond)
			}
		}
	}

	as.shutdown()

	for i := 0; i < len(test.Clients)-finishedClients; i++ {
		select {
		case cb := <-as.clientCallbacks:
			cb()
		case err = <-clientResults:
			if err != nil {
				return
			}
		}
	}

	return err
}

var logLevel = zap.LevelFlag("test.zaploglevel", zap.DebugLevel, "set log level")

func RunTests(
	t *testing.T,
	countersCtor interfaces.CountersServiceCtor,
	tests []TestDescriptor) {
	log := zaptest.NewLogger(t, zaptest.WrapOptions(zap.IncreaseLevel(logLevel))).Sugar()
	for _, test := range tests {
		log.Infow("Running", "test", test.TestName)
		t.Run(test.TestName, func(t *testing.T) {

			resultChannel := make(chan error)
			go func() {
				resultChannel <- RunTest(test, countersCtor, log)
			}()

			select {
			case err := <-resultChannel:
				if err != nil {
					log.Errorf("Test failed: %v", err)
					t.Fail()
				}

			case <-time.After(TestTimeout):
				log.Errorw("Test timed out", "timeout", TestTimeout)
				t.Fail()
			}
		})
	}
}
