package interfaces

import (
	"fmt"

	"go.uber.org/zap"
	"google.golang.org/protobuf/proto"
)

const MaxMessageSize = 100 * 1024

type Message struct {
	Payload proto.Message
	Sender  string
}

type Actor interface {
	Receive(message Message)
}

type ActorContext interface {
	Send(payload proto.Message, receiver string) error
	Log() *zap.SugaredLogger
}

func GetCounterActorId(nodeIndex int) string {
	return fmt.Sprintf("counter-%v", nodeIndex)
}

type CountersServiceCtor func(ActorContext, int, int) Actor
