package lib

import (
	"time"

	"google.golang.org/protobuf/proto"
)

const MaxTimestamp = 10000
const TestTimeout = 10 * time.Second

type SentMessage struct {
	Sender           string
	Receiver         string
	Payload          []byte
	PayloadThunk     proto.Message
	SendTimestamp    int
	DeliverTimestamp int
}

type ClientFunc func(actorSystem *ActorSystem) error

type MessageDelayFunc func(message SentMessage) int

type TestDescriptor struct {
	TestName     string
	Clients      []ClientFunc
	DelayMessage MessageDelayFunc
}
