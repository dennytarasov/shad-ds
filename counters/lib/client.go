package lib

import (
	"fmt"

	"gitlab.com/slon/shad-ds/counters/interfaces"
	"go.uber.org/atomic"
	"go.uber.org/zap"
)

func generateId() string {
	return fmt.Sprintf("request_%v", MonotonicRequestId.Inc())
}

var MonotonicRequestId atomic.Int64
var MonotonicClientId atomic.Int64

type ShutdownError struct {
}

func (e ShutdownError) Error() string {
	return "simulation finished"
}

/////////////////////////////////////////////////////////////////////////////////////////////

type RequestInfo struct {
	Id      string
	ActorId string
}

type GetResponse struct {
	Err   error
	Value int64
}

type IncrementResponseChannel chan error
type GetResponseChannel chan GetResponse

type Client struct {
	Id                     string
	OutstandingRequestInfo *RequestInfo
	OutstandingIncrement   IncrementResponseChannel
	OutstandingGet         GetResponseChannel
	ActorSystem            *ActorSystem
	ActorContext           *ActorContext

	Finished bool
}

func (c *Client) Log() *zap.SugaredLogger {
	return c.ActorContext.Log()
}

func (c *Client) Shutdown() {
	c.Finished = true

	if c.OutstandingIncrement != nil {
		responseChannel := c.OutstandingIncrement
		c.OutstandingIncrement = nil
		c.OutstandingRequestInfo = nil
		responseChannel <- ShutdownError{}
	}

	if c.OutstandingGet != nil {
		responseChannel := c.OutstandingGet
		c.OutstandingGet = nil
		c.OutstandingRequestInfo = nil
		getResponse := GetResponse{
			Err:   ShutdownError{},
			Value: 0,
		}
		responseChannel <- getResponse
	}
}

func (c *Client) Receive(message interfaces.Message) {
	switch m := message.Payload.(type) {
	case *interfaces.IncrementCounterRsp:
		c.processIncrementCounterResponse(m, message.Sender)

	case *interfaces.GetCounterRsp:
		c.processGetCounterResponse(m, message.Sender)

	default:
		panic(fmt.Errorf("client received unexpected message type %T", message))
	}
}

func (c *Client) processIncrementCounterResponse(message *interfaces.IncrementCounterRsp, sender string) {
	c.validateResponseInfo(message.RequestId, sender)
	if c.OutstandingIncrement == nil {
		panic("received increment counter response, but response channel does not exist")
	}
	c.Log().Debugw("Incremented counter",
		"request_id", message.RequestId,
		"actor_id", sender)
	responseChannel := c.OutstandingIncrement
	c.OutstandingIncrement = nil
	responseChannel <- nil
}

func (c *Client) processGetCounterResponse(message *interfaces.GetCounterRsp, sender string) {
	c.validateResponseInfo(message.RequestId, sender)
	if c.OutstandingGet == nil {
		panic("received get counter response, but response channel does not exist")
	}
	c.Log().Debugw("Got counter",
		"request_id", message.RequestId,
		"actor_id", sender)
	responseChannel := c.OutstandingGet
	c.OutstandingGet = nil
	getResponse := GetResponse{
		Err:   nil,
		Value: message.Value,
	}
	responseChannel <- getResponse
}

func (c *Client) validateConcurrentRequests() {
	if c.OutstandingRequestInfo != nil {
		panic(fmt.Errorf("client %v cannot serve concurrent requests", c.Id))
	}
}

func (c *Client) validateResponseInfo(requestId string, actorId string) {
	if c.OutstandingRequestInfo == nil {
		panic("client was not expecting response")
	}

	if c.OutstandingRequestInfo.Id != requestId {
		panic(fmt.Errorf("received response with unexpected id %q instead of %q",
			requestId,
			c.OutstandingRequestInfo.Id))
	}

	if c.OutstandingRequestInfo.ActorId != actorId {
		panic(fmt.Errorf("received response with unexpected sender id %q insted of %q",
			actorId,
			c.OutstandingRequestInfo.ActorId))
	}

	c.OutstandingRequestInfo = nil
}

/////////////////////////////////////////////////////////////////////////////////////////////

func (c *Client) IncrementCounter(counterId string, value int64, nodeIndex int) IncrementResponseChannel {
	requestId := generateId()
	responseChan := make(IncrementResponseChannel, 1)

	c.Log().Debugw("Incrementing counter",
		"request_id", requestId,
		"counter_id", counterId,
		"value", value,
		"actor_id", interfaces.GetCounterActorId(nodeIndex))

	var req interfaces.IncrementCounterReq
	req.RequestId = requestId
	req.CounterId = counterId
	req.Value = value

	callback := func() {
		c.validateConcurrentRequests()

		actorId := interfaces.GetCounterActorId(nodeIndex)

		c.OutstandingRequestInfo = &RequestInfo{Id: requestId, ActorId: actorId}
		c.OutstandingIncrement = responseChan

		if c.Finished {
			c.Shutdown()
		} else {
			err := c.ActorContext.Send(&req, actorId)
			if err != nil {
				panic(fmt.Errorf("failed to send message to %q actor, %v", actorId, err))
			}
		}
	}
	c.ActorSystem.clientCallbacks <- callback
	return responseChan
}

func (c *Client) GetCounter(counterId string, nodeIndex int) GetResponseChannel {
	requestId := generateId()
	responseChan := make(GetResponseChannel, 1)

	c.Log().Debugw("Getting counter",
		"request_id", requestId,
		"counter_id", counterId,
		"actor_id", interfaces.GetCounterActorId(nodeIndex))

	var req interfaces.GetCounterReq
	req.RequestId = requestId
	req.CounterId = counterId

	callback := func() {
		c.validateConcurrentRequests()

		actorId := interfaces.GetCounterActorId(nodeIndex)

		c.OutstandingRequestInfo = &RequestInfo{Id: requestId, ActorId: actorId}
		c.OutstandingGet = responseChan

		if c.Finished {
			c.Shutdown()
		} else {
			err := c.ActorContext.Send(&req, actorId)
			if err != nil {
				panic(fmt.Errorf("failed to send message to %q actor, %v", actorId, err))
			}
		}
	}
	c.ActorSystem.clientCallbacks <- callback
	return responseChan
}

/////////////////////////////////////////////////////////////////////////////////////////////

func NewClient(actorSystem *ActorSystem) *Client {
	clientId := fmt.Sprintf("client_%v", MonotonicClientId.Inc())
	context := ActorContext{
		actorSystem: actorSystem,
		id:          clientId,
	}
	client := Client{
		Id:           clientId,
		ActorContext: &context,
		ActorSystem:  actorSystem,
		Finished:     false,
	}
	err := actorSystem.registerClient(clientId, &client)
	if err != nil {
		panic(fmt.Errorf("failed to register new client, %v", err))
	}
	return &client
}
