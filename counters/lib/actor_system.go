package lib

import (
	"container/heap"
	"fmt"
	"sync"

	"go.uber.org/atomic"

	"gitlab.com/slon/shad-ds/counters/interfaces"
	"go.uber.org/zap"
	"google.golang.org/protobuf/proto"
)

type ActorSystem struct {
	delayMessage           MessageDelayFunc
	actors                 map[string]interfaces.Actor
	actorIsolated          map[string]struct{}
	clients                map[string]*Client
	sentMessages           chan SentMessage
	clientCallbacks        chan func()
	queuedMessages         MessageQueue
	timestamp              *atomic.Int32
	timestampDeliveryLimit *atomic.Int32
	actorMutex             sync.Mutex
	clientMutex            sync.Mutex
	log                    *zap.SugaredLogger
}

func (as *ActorSystem) IsolateActor(id string) error {
	as.actorMutex.Lock()
	defer as.actorMutex.Unlock()

	if _, ok := as.actors[id]; !ok {
		return fmt.Errorf("actor %q does not exist", id)
	}

	as.actorIsolated[id] = struct{}{}

	return nil
}

func (as *ActorSystem) SetTimestampDeliveryLimitFromCurrent(delta int32) {
	as.timestampDeliveryLimit.Store(as.timestamp.Load() + delta)
}

func (as *ActorSystem) ResetTimestampDeliveryLimit() {
	as.timestampDeliveryLimit.Store(-1)
}

func (as *ActorSystem) registerActor(id string, actor interfaces.Actor) (err error) {
	as.actorMutex.Lock()
	defer as.actorMutex.Unlock()

	if _, ok := as.actors[id]; ok {
		return fmt.Errorf("actor %q already exists", id)
	}

	as.actors[id] = actor
	return err
}

func (as *ActorSystem) getActor(id string) (interfaces.Actor, bool) {
	as.actorMutex.Lock()
	defer as.actorMutex.Unlock()
	actor := as.actors[id]
	_, isolated := as.actorIsolated[id]
	return actor, isolated
}

func (as *ActorSystem) registerClient(id string, client *Client) (err error) {
	as.clientMutex.Lock()
	defer as.clientMutex.Unlock()
	if _, ok := as.clients[id]; ok {
		return fmt.Errorf("client %q already exists", id)
	}

	if err := as.registerActor(id, client); err != nil {
		return err
	}

	as.clients[id] = client
	return err
}

func (as *ActorSystem) actorExists(id string) bool {
	as.actorMutex.Lock()
	defer as.actorMutex.Unlock()

	_, ok := as.actors[id]
	return ok
}

func (as *ActorSystem) clientExists(id string) bool {
	as.clientMutex.Lock()
	defer as.clientMutex.Unlock()

	_, ok := as.clients[id]
	return ok
}

func (as *ActorSystem) enqueueSentMessage(sentMessage SentMessage) {
	sentMessage.SendTimestamp = int(as.timestamp.Load())
	sentMessage.DeliverTimestamp = sentMessage.SendTimestamp + 1

	if !as.clientExists(sentMessage.Sender) && !as.clientExists(sentMessage.Receiver) {
		sentMessage.DeliverTimestamp = as.delayMessage(sentMessage)
	}

	heap.Push(&as.queuedMessages, sentMessage)
}

func (as *ActorSystem) deliverNextMessage() bool {
	if len(as.queuedMessages) == 0 {
		return false
	}

	timestampDeliveryLimit := int(as.timestampDeliveryLimit.Load())
	if timestampDeliveryLimit >= 0 && as.queuedMessages[0].DeliverTimestamp >= timestampDeliveryLimit {
		return false
	}

	sentMessage := heap.Pop(&as.queuedMessages).(SentMessage)

	as.timestamp.Store(int32(sentMessage.DeliverTimestamp))

	payload := proto.Clone(sentMessage.PayloadThunk)
	if err := proto.Unmarshal(sentMessage.Payload, payload); err != nil {
		panic(err)
	}

	message := interfaces.Message{
		Payload: payload,
		Sender:  sentMessage.Sender}

	actor, actorIsolated := as.getActor(sentMessage.Receiver)
	if !actorIsolated {
		actor.Receive(message)
	}

	return true
}

func (as *ActorSystem) getCurrentTimestamp() int {
	return int(as.timestamp.Load())
}

func (as *ActorSystem) shutdown() {
	for _, client := range as.clients {
		client.Shutdown()
	}
}
