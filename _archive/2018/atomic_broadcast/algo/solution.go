package algo

import "gitlab.com/slon/shad-ds/atomic_broadcast/lib"

var HasSolution bool = false

func NewSolution(nodeId, nodeCount int, deliver lib.Deliver, context lib.Context) lib.AtomicBroadcaster {
	return NewExample(nodeId, nodeCount, deliver, context)
}
