package tests

import (
	"fmt"
	"time"

	"gitlab.com/slon/shad-ds/counters/interfaces"
	"gitlab.com/slon/shad-ds/counters/lib"
)

func expectValue(rsp lib.GetResponse, value int64) error {
	if rsp.Err != nil {
		return fmt.Errorf("expected counter value %v, but got error %v", value, rsp.Err)
	}
	if rsp.Value != value {
		return fmt.Errorf("expected counter value %v, but got %v", value, rsp.Value)
	}
	return nil
}

func expectOneOfValue(rsp lib.GetResponse, values []int64) error {
	if rsp.Err != nil {
		return fmt.Errorf("expected some counter value from %v, but got error %v", values, rsp.Err)
	}
	for _, value := range values {
		if value == rsp.Value {
			return nil
		}
	}
	return fmt.Errorf("expected some counter value from %v, but got %v", values, rsp.Value)

}

/////////////////////////////////////////////////////////////////////////////////////////////

func isIn(value int, values []int) bool {
	for _, v := range values {
		if v == value {
			return true
		}
	}
	return false
}

func simpleClient(as *lib.ActorSystem, nodeCount int) error {
	c := lib.NewClient(as)

	counterCount := 2
	sum := make([]int64, counterCount)

	f := func(isolatedNodeIndexes []int) error {
		for counterIndex := 0; counterIndex < counterCount; counterIndex++ {
			name := fmt.Sprintf("mycounter-%v", counterIndex)

			additives := []int64{1, 1, -2, -3, 8, 0}
			incrementNodeIndex := 0
			for _, additive := range additives {
				for nodeIndex := 0; nodeIndex < nodeCount; nodeIndex++ {
					if isIn(nodeIndex, isolatedNodeIndexes) {
						continue
					}
					rsp := <-c.GetCounter(name, nodeIndex)
					if err := expectValue(rsp, sum[counterIndex]); err != nil {
						return err
					}
				}
				sum[counterIndex] += additive
				for isIn(incrementNodeIndex, isolatedNodeIndexes) {
					incrementNodeIndex = (incrementNodeIndex + 1) % nodeCount
				}
				if err := <-c.IncrementCounter(name, additive, incrementNodeIndex); err != nil {
					return err
				}
				incrementNodeIndex = (incrementNodeIndex + 1) % nodeCount
			}
		}
		return nil
	}

	isolatedNodexIndexes := make([]int, 0)
	for 2*(nodeCount-len(isolatedNodexIndexes)) > nodeCount {
		if err := f(isolatedNodexIndexes); err != nil {
			return err
		}
		i := len(isolatedNodexIndexes)
		as.IsolateActor(interfaces.GetCounterActorId(i))
		isolatedNodexIndexes = append(isolatedNodexIndexes, i)
	}

	return nil
}

func createSimpleTest(nodeCount int) lib.TestDescriptor {
	fixedDelay := func(message lib.SentMessage) int {
		return message.SendTimestamp + 5
	}

	return lib.TestDescriptor{
		TestName: fmt.Sprintf("Simple test with %v node(s)", nodeCount),
		Clients: []lib.ClientFunc{func(as *lib.ActorSystem) error {
			return simpleClient(as, nodeCount)
		}},
		NodeCount:    nodeCount,
		DelayMessage: fixedDelay,
	}
}

/////////////////////////////////////////////////////////////////////////////////////////////

func simpleIsolationClient(as *lib.ActorSystem) error {
	c := lib.NewClient(as)

	// Client<>node communication is free, but node<>node is blocked.
	as.SetTimestampDeliveryLimitFromCurrent(50)

	rspChan := c.IncrementCounter("mycounter", 10, 0)
	time.Sleep(25 * time.Millisecond)
	select {
	case err := <-rspChan:
		if err != nil {
			return fmt.Errorf("unepxected error: %v", err)
		}
		// Implementation lacks needed synchronization between nodes,
		// because request was processed without any node<>node communication.
		// Let's prove it!
		for nodeIndex := 1; nodeIndex < 3; nodeIndex++ {
			rsp := <-c.GetCounter("mycounter", nodeIndex)
			if err := expectValue(rsp, 10); err != nil {
				return err
			}
		}
	default:
		c = lib.NewClient(as)

		// Implementation has synchronization between nodes,
		// because request processing requires node<>node communication.
		// Let's ensure that already sent requests will be processed properly.
		as.IsolateActor(interfaces.GetCounterActorId(0))
		as.ResetTimestampDeliveryLimit()
		var lastValue int64 = 0
		for i := 0; i < 10; i++ {
			for nodeIndex := 1; nodeIndex < 3; nodeIndex++ {
				rsp := <-c.GetCounter("mycounter", nodeIndex)
				expectedValues := make([]int64, 2)
				if lastValue == 0 {
					expectedValues = append(expectedValues, 0)
				}
				expectedValues = append(expectedValues, 10)
				if err := expectOneOfValue(rsp, expectedValues); err != nil {
					return nil
				}
				lastValue = rsp.Value
			}
			time.Sleep(10 * time.Millisecond)
		}
	}

	return nil
}

func createSimpleIsolationTest() lib.TestDescriptor {
	fixedDelay := func(message lib.SentMessage) int {
		return message.SendTimestamp + 100
	}

	return lib.TestDescriptor{
		TestName:     "Simple isolation test",
		Clients:      []lib.ClientFunc{simpleIsolationClient},
		NodeCount:    3,
		DelayMessage: fixedDelay,
	}
}

/////////////////////////////////////////////////////////////////////////////////////////////

var PublicTests = []lib.TestDescriptor{
	createSimpleTest(2),
	createSimpleTest(3),
	createSimpleTest(4),
	createSimpleTest(5),

	createSimpleIsolationTest(),
}
